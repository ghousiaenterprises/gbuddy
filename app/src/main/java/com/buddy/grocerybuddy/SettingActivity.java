package com.buddy.grocerybuddy;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.buddy.grocerybuddy.lib.C;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

public class SettingActivity extends AppCompatActivity {

    private static final String SCREEN_OFF_TIMEOUT = "screen_time_out";
    private CheckBox displayTimeCheckbox, displayRotationCheckbox;
    private SharedPreferences sf;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("    Settings");
        toolbar.setTitleTextAppearance(this, R.style.Tootbar_TitleText);
        toolbar.setLogo(R.mipmap.settings);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        sf = getSharedPreferences(getPackageName() + C.PREF_FILE_NAME, MODE_PRIVATE);

        displayTimeCheckbox = (CheckBox) findViewById(R.id.display_checkbox);
        displayRotationCheckbox = (CheckBox) findViewById(R.id.rotation_checkbox);

        displayRotationCheckbox.setChecked(true);
        displayTimeCheckbox.setChecked(true);

        saveSettingsOnChange();
        setDefaultSettingValues();

        applySettings();

    }

    @Override
    protected void onResume() {
        super.onResume();
        applySettings();
    }

    private void applySettings() {

        if (sf.getBoolean(C.DISPLAY_ROTATION, true)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        if (sf.getBoolean(C.DISPLAY_TIME_OUT, true)) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }

    }

    private void setDefaultSettingValues() {
        displayTimeCheckbox.setChecked(sf.getBoolean(C.DISPLAY_TIME_OUT, false));
        displayRotationCheckbox.setChecked(sf.getBoolean(C.DISPLAY_ROTATION, false));
    }

    private void saveSettingsOnChange() {

        displayTimeCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                SharedPreferences.Editor editor = sf.edit();
                editor.putBoolean(C.DISPLAY_TIME_OUT, isChecked);
                editor.apply();
                if (isChecked)
                    getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                else
                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

            }
        });

        displayRotationCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                SharedPreferences.Editor editor = sf.edit();
                editor.putBoolean(C.DISPLAY_ROTATION, isChecked);
                editor.apply();
                if (isChecked)
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                else
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
            }
        });
    }

    public void manageProduced_clickHandler(View view) {
        startActivity( new Intent(SettingActivity.this, ManageProducedActivity.class));
    }

    public void open_category_list_clickedHandler(View view){
        startActivity( new Intent(SettingActivity.this, CategoryActivity.class) );
    }

}
