package com.buddy.grocerybuddy.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.buddy.grocerybuddy.R;
import com.buddy.grocerybuddy.models.ProduceItem;
import com.buddy.grocerybuddy.models.ShoppingList;

import java.util.List;

import io.realm.Realm;

/**
 * Created by Arif Iqbal on 22-May-17.
 */

public class ShoppingRecyclerAdapter extends RecyclerView.Adapter<ShoppingRecyclerAdapter.MyViewHolder> {

    private List<ProduceItem> dataList;
    private Realm mRealm;
    private LayoutInflater mInflater;

    public ShoppingRecyclerAdapter(Context context, List<ProduceItem> data, Realm reamDatabaseInstence) {
        this.dataList = data;
        this.mInflater = LayoutInflater.from(context);
        mRealm = reamDatabaseInstence;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.shopping_grocery_list_item, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        ProduceItem currentItem = dataList.get(position);
        holder.setData(currentItem, position);
        holder.setListener();
    }

    public void updateAdapter(List<ProduceItem> data) {
        this.dataList = data;
        notifyDataSetChanged();
    }

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView title, subtitle;
        private ImageButton starred;
        private int position;
        private ProduceItem current;
        private RelativeLayout container;
        private String currentCategoryId = "";
        private int currentBgResource;

        MyViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.shopping_list_title);
            subtitle = (TextView) itemView.findViewById(R.id.shopping_list_subtitle);
            starred = (ImageButton) itemView.findViewById(R.id.shopping_list_starred);
            container = (RelativeLayout) itemView.findViewById(R.id.shopping_item_container);
        }

        void setData(ProduceItem i, int position) {
            title.setText(i.getName());
            if (i.getDescription() != null && !i.getDescription().equals("")){ subtitle.setText(" (" + i.getDescription() + ")"); }
            else{ subtitle.setText(""); }

            if (i.isShoppingCompleted()) {
                Paint p = new Paint();
                p.setStrokeWidth(10);
                title.setLayerPaint(p);
                title.setPaintFlags(title.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG );
            }else{
                title.setPaintFlags(0);
            }

            if ( i.isStarred() ) {
//                    GradientDrawable drawable = new GradientDrawable();
//                    drawable.setCornerRadius(15);
//                    float[] f = {200,0,200,0};
//                    drawable.setCornerRadii(f);
//                    drawable.setColor(Color.parseColor("#b61c1c"));
//                    container.setBackground(drawable);
                starred.setVisibility(View.VISIBLE);
            } else {
                starred.setVisibility(View.GONE);
            }

            setBackgroundResource(i.getShoppingListId());
            this.position = position;
            this.current = i;
        }

        public void setBackgroundResource(String categoryId) {
            if (!currentCategoryId.equals(categoryId)) {
                ShoppingList category = mRealm.where(ShoppingList.class).equalTo("id", categoryId).findFirst();
                currentBgResource = category.getProducedBackground();
            }
            container.setBackgroundResource(currentBgResource);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.shopping_item_container:
                    markAsCompleted(position);
                    break;
                case R.id.shopping_card_view_down_btn:

                    break;
            }
        }

        void setListener() {

            container.setOnClickListener(this);

        }

        public void markAsCompleted(final int position){
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    ProduceItem oldItem = dataList.get(position);
                    ProduceItem item = realm.where(ProduceItem.class).equalTo("id", dataList.get(position).getId()).findFirst();
                    item.setShoppingCompleted(!oldItem.isShoppingCompleted());
                }
            });
            dataList.remove(position);
            notifyItemRemoved(position);
            notifyItemRangeChanged(position, dataList.size());
        }

    }

}
