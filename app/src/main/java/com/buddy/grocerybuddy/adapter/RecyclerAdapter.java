    package com.buddy.grocerybuddy.adapter;

    import android.content.Context;
    import android.support.v7.widget.LinearLayoutManager;
    import android.support.v7.widget.RecyclerView;
    import android.view.LayoutInflater;
    import android.view.View;
    import android.view.ViewGroup;
    import android.widget.ImageButton;
    import android.widget.ImageView;
    import android.widget.TextView;

    import com.buddy.grocerybuddy.R;
    import com.buddy.grocerybuddy.models.ShoppingList;

    import java.util.Collections;

    import io.realm.Case;
    import io.realm.Realm;
    import io.realm.RealmResults;

    /**
     * Created by Arif Iqbal on 22-May-17.
     */

    public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.MyViewHolder> {

        private LinearLayoutManager layoutManager;
        private RealmResults<ShoppingList> dataList;
        private Realm mRealm;
        private LayoutInflater mInflater;

        public RecyclerAdapter(Context context, RealmResults<ShoppingList> data, Realm reamDatabaseInstence, LinearLayoutManager layoutManager){
            this.dataList = data;
            this.mInflater = LayoutInflater.from(context);
            mRealm = reamDatabaseInstence;
            this.layoutManager = layoutManager;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = mInflater.inflate(R.layout.category_list_item, parent,false);
            MyViewHolder holder = new MyViewHolder(view);
            return holder;
        }

        @Override
        public int getItemCount() {
            return dataList.size();
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {
            ShoppingList currentItem = dataList.get(position);
            holder.setData(currentItem, position);
            holder.setListener();
        }

        public void removeListItem(int position){
            Collections.swap(dataList, position, position+1);
            notifyItemRangeChanged(position, dataList.size());
        }

        public void moveItemDown(int position){
            if( position == (dataList.size()-1) ) { return; }
            int oi = position;
            int ni = position+1;
            updateItemPosition(oi, ni);
        }

        public void moveItemUp(int position){
            if( position == 0 ) { return; }
            int oi = position;
            int ni = position-1;
            updateItemPosition(ni, oi);
        }

        public void addListItem(int position){
    //        dataList.add(dataList.get(position));
    //        notifyItemInserted(position);
    //        notifyItemRangeChanged(position, dataList.size());
            Collections.swap(dataList, position, position-1);
            notifyItemRangeChanged(position, dataList.size());
    //        notifyItemMoved(position,position-1);
        }

        private void updateItemPosition(final int pos, final int new_pos){
            final String current_item_id = dataList.get(pos).getId();
            final String new_item_id = dataList.get(new_pos).getId();
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    ShoppingList old_item = realm.where(ShoppingList.class)
                            .equalTo("id", current_item_id, Case.INSENSITIVE)
                            .findFirst();
                    old_item.setPosition(new_pos);

                    ShoppingList new_item = realm.where(ShoppingList.class)
                            .equalTo("id", new_item_id, Case.INSENSITIVE)
                            .findFirst();
                    new_item.setPosition(pos);

                    if( pos == 0 || new_pos == 1 )
                        layoutManager.scrollToPosition(0);

                    notifyItemMoved(pos, new_pos);
                    notifyItemRangeChanged(pos , dataList.size());
                }
            });
        }

        class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

            TextView title;
            ImageButton up, down;
            ImageView image;
            int position;
            ShoppingList current;

            MyViewHolder(View itemView) {
                super(itemView);
                title = (TextView) itemView.findViewById(R.id.shopping_card_view_title);
                image = (ImageView) itemView.findViewById(R.id.shopping_card_view_image);
                up = (ImageButton) itemView.findViewById(R.id.shopping_card_view_up_btn);
                down = (ImageButton) itemView.findViewById(R.id.shopping_card_view_down_btn);
            }

            void setData(ShoppingList i, int position) {
                title.setText( i.getTitle() );
                image.setImageResource(i.getImageId());
                up.setBackgroundResource(i.getColor());
                down.setBackgroundResource(i.getColor());

                this.position = position;
                this.current = i;
            }

            @Override
            public void onClick(View view) {
                switch (view.getId()){
                    case R.id.shopping_card_view_up_btn:
                        moveItemUp(position);
                        break;
                    case R.id.shopping_card_view_down_btn:
                        moveItemDown(position);
                        break;
                }
            }

            void setListener() {
                up.setOnClickListener(MyViewHolder.this);
                down.setOnClickListener(MyViewHolder.this);
            }
        }

    }
