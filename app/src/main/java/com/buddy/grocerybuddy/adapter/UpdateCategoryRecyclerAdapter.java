    package com.buddy.grocerybuddy.adapter;

    import android.content.Context;
    import android.support.v7.widget.RecyclerView;
    import android.util.Log;
    import android.view.LayoutInflater;
    import android.view.View;
    import android.view.ViewGroup;
    import android.view.inputmethod.InputMethodManager;
    import android.widget.ImageView;
    import android.widget.LinearLayout;
    import android.widget.TextView;

    import com.buddy.grocerybuddy.R;
    import com.buddy.grocerybuddy.UpdateGroceryActivity;
    import com.buddy.grocerybuddy.models.ProduceItem;
    import com.buddy.grocerybuddy.models.ShoppingList;

    import io.realm.Realm;
    import io.realm.RealmResults;

    /**
     * Created by Arif Iqbal on 22-May-17.
     */

    public class UpdateCategoryRecyclerAdapter extends RecyclerView.Adapter<UpdateCategoryRecyclerAdapter.MyViewHolder> {

        private RealmResults<ShoppingList> data;
        private LayoutInflater mInflater;
        private Boolean displayText = false;
        private Realm mRealm;
        private RecyclerView producedRecycler;
        private UpdateProducedRecyclerAdapter producedAdapter;
        private LinearLayout producedActionContainer;
        private Context ctx;

        public UpdateCategoryRecyclerAdapter(Context context,
                                             RealmResults<ShoppingList> data,
                                             UpdateProducedRecyclerAdapter producedAdapter,
                                             RecyclerView producedRecyclerView,
                                             Realm mRealmInstance,
                                             LinearLayout container){
            this.data = data;
            this.ctx = context;
            this.mInflater = LayoutInflater.from(context);
            this.mRealm = mRealmInstance;
            this.producedRecycler = producedRecyclerView;
            this.producedAdapter = producedAdapter;
            this.producedActionContainer = container;

            producedRecycler.setBackgroundResource( data.get(0).getColor() );
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = mInflater.inflate(R.layout.update_grocery_category_list_item, parent,false);
            MyViewHolder holder = new MyViewHolder(view);
            return holder;
        }

        @Override
        public int getItemCount() {
            return data.size();
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {
            ShoppingList currentItem = data.get(position);
            holder.setData(currentItem, position);
        }

        public void swapView(boolean show){
            displayText = show;
            notifyDataSetChanged();
        }

        class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

            TextView title;
            ImageView image;
            LinearLayout container;
            int position;
            ShoppingList current;

            MyViewHolder(View itemView) {
                super(itemView);
                title = (TextView) itemView.findViewById(R.id.shopping_card_view_title);
                image = (ImageView) itemView.findViewById(R.id.shopping_card_view_image);
                container = (LinearLayout) itemView.findViewById(R.id.update_grocery_item_container);
                container.setOnClickListener(this);
            }

            void setData(ShoppingList i, int position) {

                title.setText( (displayText)? i.getTitle():"" );
                image.setImageResource(i.getImageId());
                container.setBackgroundResource(i.getBorder());
                this.position = position;
                this.current = i;

            } // bind data to view

            @Override
            public void onClick(View view) {

                switch (view.getId()){

                    case R.id.update_grocery_item_container:
                        RealmResults<ProduceItem> data = null;
                        if ( current.getTitle().equalsIgnoreCase("Favorite") ){
                            data = mRealm.where(ProduceItem.class).equalTo("starred", true).findAll();
                        }else{
                            data = mRealm.where(ProduceItem.class).equalTo("shoppingListId", current.getId()).findAll();
                        }
                        producedRecycler.setBackgroundResource( current.getColor() );
                        producedActionContainer.setBackgroundResource(current.getColor());
                        producedAdapter.updateData(data, current);
                        if (UpdateGroceryActivity.keyboardShowing){
                            InputMethodManager imm = (InputMethodManager)   ctx.getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
                            UpdateGroceryActivity.keyboardShowing = false;
                        }
                        break;

                } // Switch item clicked

            } // onClick Implementation.

        } // viewHolder class

    }
