package com.buddy.grocerybuddy.lib;

import android.content.SharedPreferences;

public class C {

    public static final String PREF_FILE_NAME = ".grocery_buddy";
    public static final String DISPLAY_TIME_OUT = "display_time_out";
    public static final String DISPLAY_LOCK = "display_lock";
    public static final String DISPLAY_ROTATION = "display_rotation";

    public static final String NOTES_EDITOR = "note_editor_text";


}
