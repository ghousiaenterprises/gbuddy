package com.buddy.grocerybuddy;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.buddy.grocerybuddy.lib.C;
import com.buddy.grocerybuddy.models.ProduceItem;
import com.buddy.grocerybuddy.models.ShoppingList;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import io.realm.Case;
import io.realm.Realm;
import io.realm.RealmResults;

public class ManageProducedActivity extends AppCompatActivity {

    private EditText editTextProduced;
    private String[] shoppingListIds;
    private String[] spinnerData;
    private ProgressBar loader;
    private ImageButton addProduceBtn;
    private String currentShoppingListSelectedID;
    private String[] currentListViewSelectedIDs;
    private Realm myRealm;
    private ListView produceList;
    private SharedPreferences sf;
    private AdView mAdView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_produced);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setLogo(R.mipmap.manage_produced);
        toolbar.setTitleMargin(50,0,50,0);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        sf = getSharedPreferences(getPackageName() + C.PREF_FILE_NAME, Context.MODE_PRIVATE);
        init_view();

        initSpringDataWithId();

        Spinner spinner = (Spinner) findViewById(R.id.manage_produced_spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.planets_array, android.R.layout.simple_spinner_item);
        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, spinnerData);
        spinner.setAdapter(adapter1);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                currentShoppingListSelectedID = shoppingListIds[i];
                updateListBySelectedShoppingList(currentShoppingListSelectedID);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    private void init_view() {
        loader = (ProgressBar) findViewById(R.id.progressBar);
        editTextProduced = (EditText) findViewById(R.id.add_produced_text_editor);

        removeInputFocusable();

        addProduceBtn = (ImageButton) findViewById(R.id.add_produced_button);
        produceList = (ListView) findViewById(R.id.produced_list_view);

        myRealm = Realm.getDefaultInstance();

    }

    private void removeInputFocusable() {
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    public void add_produced_item_button_clicked(View view) {
        if( editTextProduced.getText().toString().trim().length() == 0 ){
            Toast.makeText(this, "Please Type Name", Toast.LENGTH_LONG).show();
            return;
        }

        loader.setVisibility(View.VISIBLE);
        addProduceBtn.setEnabled(false);

        myRealm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {

                String id = UUID.randomUUID().toString();
                ProduceItem pItem = myRealm.createObject(ProduceItem.class, id);
                pItem.setName(editTextProduced.getText().toString());
                pItem.setShoppingListId(currentShoppingListSelectedID);
                removeInputFocusable();
            }
        });

        updateListBySelectedShoppingList(currentShoppingListSelectedID);

        editTextProduced.setText("");
        addProduceBtn.setEnabled(true);
        loader.setVisibility(View.GONE);
    }

    private void initSpringDataWithId(){
        RealmResults<ShoppingList> shoppingLists = myRealm.where(ShoppingList.class)
                .notEqualTo("title","Favorite", Case.INSENSITIVE).findAll();

        int len = shoppingLists.size();
        spinnerData = new String[len];
        shoppingListIds = new String[len];

        for ( int i = 0; i < shoppingLists.size(); i++ ){
            ShoppingList item = shoppingLists.get(i);
            spinnerData[i] = item.getTitle();
            shoppingListIds[i] = item.getId();
            if( i == 0 ) { currentShoppingListSelectedID = item.getId(); };
        }
        loader.setVisibility(View.GONE);
    }

    private void updateListBySelectedShoppingList(String id){
        RealmResults<ProduceItem> produceItem = myRealm.where(ProduceItem.class).equalTo("shoppingListId", id).findAll();
        List<String> lists = new ArrayList<>();
        currentListViewSelectedIDs = new String[produceItem.size()];
        int i = 0;

        if(produceItem != null){
            for( ProduceItem subItem : produceItem ){
                lists.add(subItem.getName());
                currentListViewSelectedIDs[i] = subItem.getId();
            }
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, lists );
        produceList.setAdapter(adapter);
    }

    private void applySettings() {

        if (sf.getBoolean(C.DISPLAY_ROTATION, false)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        if (sf.getBoolean(C.DISPLAY_TIME_OUT, false)) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        myRealm.close();
    }

    @Override
    protected void onResume() {
        super.onResume();
        applySettings();
    }
}
