package com.buddy.grocerybuddy.lib;

import android.util.Log;

import io.realm.DynamicRealm;
import io.realm.FieldAttribute;
import io.realm.RealmMigration;
import io.realm.RealmObject;
import io.realm.RealmObjectSchema;
import io.realm.RealmSchema;

/**
 * Created by Arif Iqbal on 24-May-17.
 */

public class MyRealmMigration implements RealmMigration {
    @Override
    public void migrate(DynamicRealm realm, long oldVersion, long newVersion) {

        RealmSchema schema = realm.getSchema();

//        if (oldVersion == 0){
//            RealmObjectSchema shoppingList = schema.get("ShoppingList");
//            shoppingList.addField("id", String.class, FieldAttribute.PRIMARY_KEY);
//            oldVersion++;
//        }
//
//        if (oldVersion == 1){
//            RealmObjectSchema shoppingList = schema.get("ShoppingList");
//            shoppingList.addField("color", Integer.class);
//            oldVersion++;
//        }
//
//        if (oldVersion == 2){
//            RealmObjectSchema produceItem = schema.create("ProduceItem");
//
//            produceItem.addField("id", String.class, FieldAttribute.PRIMARY_KEY);
//            produceItem.addField("name", String.class, FieldAttribute.REQUIRED);
//            produceItem.addField("shoppingListId", String.class, FieldAttribute.REQUIRED);
//
//            oldVersion++;
//        }
//
//        if (oldVersion == 4){
//
//            RealmObjectSchema produceItem = schema.get("ProduceItem");
//
//            produceItem.addField("stared", Integer.class);
//            produceItem.addField("description", String.class);
//            produceItem.addField("selected", String.class);
//
//            produceItem.setNullable("started", true);
//
//            oldVersion++;
//
//        }
//
//        if (oldVersion == 6){
//
//            RealmObjectSchema produceItem = schema.get("ProduceItem");
//            produceItem.removeField("stared");
//            produceItem.addField("stared", Integer.class);
//            produceItem.setNullable("stared", true);
//            oldVersion++;
//
//        }
//
//        if (oldVersion == 7){
//
//            RealmObjectSchema shoppingList = schema.get("ShoppingList");
//            shoppingList.addField("border", Integer.class);
//            shoppingList.setNullable("border", true);
//            oldVersion++;
//
//        }

        if (oldVersion == 0){
            RealmObjectSchema producedImodel = schema.get("ProducedItem");
            producedImodel.addField("shoppingCompleted", Boolean.class);
            //noinspection UnusedAssignment
            oldVersion++;
        }

    }

}
