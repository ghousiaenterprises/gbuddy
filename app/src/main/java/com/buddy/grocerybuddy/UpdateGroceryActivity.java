package com.buddy.grocerybuddy;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.buddy.grocerybuddy.adapter.UpdateCategoryRecyclerAdapter;
import com.buddy.grocerybuddy.adapter.UpdateProducedRecyclerAdapter;
import com.buddy.grocerybuddy.lib.C;
import com.buddy.grocerybuddy.lib.OnSwipeTouchListener;
import com.buddy.grocerybuddy.models.ProduceItem;
import com.buddy.grocerybuddy.models.ShoppingList;

import java.util.UUID;

import io.realm.Case;
import io.realm.Realm;
import io.realm.RealmResults;

public class UpdateGroceryActivity extends AppCompatActivity {

    private Realm mReal;
    private RecyclerView producedRecyclerView;
    private RecyclerView categoryRecyclerView;
    private UpdateCategoryRecyclerAdapter categoryAdapter;
    private UpdateProducedRecyclerAdapter producedAdapter;
    public static ShoppingList currentSelectedCategory;
    public LinearLayout produced_bottom_container;
    private int smallWidth = 100, largeWidth = 160;
    private SharedPreferences sf;
    private boolean expended = false;
    private TextView category;
    private ImageButton categoryDownBtn;
    private EditText searchInput;
    private ImageView clearSearch;
    public static boolean keyboardShowing = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_grocery);
        setupToolbar();
        sf = getSharedPreferences(getPackageName() + C.PREF_FILE_NAME, MODE_PRIVATE);

        setupRecyclerView();

        category = (TextView) findViewById(R.id.category_text_view);
        categoryDownBtn = (ImageButton) findViewById(R.id.categoryDownButton);

        categoryRecyclerView.setOnTouchListener(new OnSwipeTouchListener(this){
            @Override
            public void onSwipeLeft() {
                openCategoryListView(category, categoryDownBtn, smallWidth, false);
            }
            @Override
            public void onSwipeRight() {
                openCategoryListView(category, categoryDownBtn, largeWidth, true);
            }
        });
        producedRecyclerView.setOnTouchListener(new OnSwipeTouchListener(this){
            @Override
            public void onSwipeLeft() {
                openCategoryListView(category, categoryDownBtn, smallWidth, false);
            }
            @Override
            public void onSwipeRight() {
                openCategoryListView(category, categoryDownBtn, largeWidth, true);
            }
        });

        applySettings();

        setupSearchInput();

    }

    private void setupSearchInput() {
        searchInput = (EditText) findViewById(R.id.searchItem);
        searchInput.setCursorVisible(false);
        searchInput.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                keyboardShowing = true;
                searchInput.setCursorVisible(true);
            }
        });

        clearSearch = (ImageView) findViewById(R.id.clear_search);

        clearSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchInput.setText("");
            }
        });

        searchInput.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                RealmResults<ProduceItem> items = mReal.where(ProduceItem.class)
                        .contains("name", searchInput.getText().toString(), Case.INSENSITIVE).findAll();
                producedAdapter.updateData(items);
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (searchInput.getText().toString().trim().equals("")){
                    clearSearch.setVisibility(View.GONE);
                }else{
                    clearSearch.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    private void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setBackgroundResource(R.color.list_blue);
        toolbar.setTitle("Update Grocery List");
        toolbar.setNavigationIcon(R.mipmap.btn_ugl);
        setSupportActionBar(toolbar);
        getWindow().getDecorView().setBackgroundColor(Color.parseColor("#eceff1"));
    }

    private void openCategoryListView(TextView category, ImageButton categoryDownBtn, int smallWidth, boolean show) {
        ViewGroup.LayoutParams lp = categoryRecyclerView.getLayoutParams();
        lp.width = getDpsValue(UpdateGroceryActivity.this, smallWidth);
        categoryRecyclerView.setLayoutParams(lp);

        ViewGroup.LayoutParams clp = category.getLayoutParams();
        clp.width = getDpsValue(UpdateGroceryActivity.this, smallWidth);
        category.setLayoutParams(clp);

        ViewGroup.LayoutParams blp = categoryDownBtn.getLayoutParams();
        blp.width = getDpsValue(UpdateGroceryActivity.this, smallWidth);
        categoryDownBtn.setLayoutParams(blp);

        categoryAdapter.swapView(show);
    }

    private void setupRecyclerView() {

        mReal = Realm.getDefaultInstance();

        RealmResults<ShoppingList> categoryDataSet = mReal.where(ShoppingList.class)
                .notEqualTo("title", "Favorite", Case.INSENSITIVE).findAll();
        ShoppingList firstCategory = categoryDataSet.get(0);
        RealmResults<ProduceItem> producedDataSet = mReal.where(ProduceItem.class)
                .equalTo("shoppingListId",firstCategory.getId()).findAll();

        currentSelectedCategory = firstCategory;
        produced_bottom_container = (LinearLayout) findViewById(R.id.produced_bottom_container);

        LinearLayoutManager categoryLayoutManager = new LinearLayoutManager(this);
        categoryLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        LinearLayoutManager producedLayoutManager = new LinearLayoutManager(this);
        producedLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        producedRecyclerView = (RecyclerView) findViewById(R.id.produced_recycler_view);
        producedAdapter = new UpdateProducedRecyclerAdapter(this, producedDataSet, mReal);
        producedRecyclerView.setAdapter(producedAdapter);
        producedRecyclerView.setLayoutManager(producedLayoutManager);

        categoryRecyclerView = (RecyclerView) findViewById(R.id.category_recycler_view);
        categoryAdapter = new UpdateCategoryRecyclerAdapter(this, categoryDataSet, producedAdapter, producedRecyclerView, mReal, produced_bottom_container);
        categoryRecyclerView.setAdapter(categoryAdapter);

        categoryRecyclerView.setLayoutManager(categoryLayoutManager);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mReal.close();
    }

    @Override
    protected void onResume() {
        super.onResume();
        applySettings();
    }

    public void add_produced_btn_clicked_handler(View view) {

        final Dialog dialog;
        final AlertDialog.Builder builder = new AlertDialog.Builder(UpdateGroceryActivity.this);

        View inflate = LayoutInflater.from(UpdateGroceryActivity.this).inflate(R.layout.custom_add_produced_dialog, null);
        builder.setView(inflate);
        builder.create();
        dialog = builder.show();

        final EditText editor = (EditText) inflate.findViewById(R.id.description_text_editer);
        TextView dialogTitle = (TextView) inflate.findViewById(R.id.dialog_title);
        dialogTitle.setText(currentSelectedCategory.getTitle());
        ImageButton saveBtn = (ImageButton) inflate.findViewById(R.id.save_description_btn);
        ImageButton cancelBtn = (ImageButton) inflate.findViewById(R.id.cancel_description_btn);

//        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                insertProducedItem(currentSelectedCategory, editor.getText().toString());
                if (dialog != null) { dialog.dismiss(); }
                producedAdapter.notifyDataSetChanged();
                toggleKeyboard(false);
            }
        });
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (dialog != null) { dialog.dismiss(); }
                toggleKeyboard(false);
            }
        });
        toggleKeyboard(true);
    }

    private void toggleKeyboard(boolean show){
        if (show){
            InputMethodManager imm = (InputMethodManager)   getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
        }else{
            InputMethodManager imm = (InputMethodManager)   getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.HIDE_NOT_ALWAYS, 0);
        }
    }

    private void insertProducedItem(final ShoppingList category, final String s) {
        mReal.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                String id = UUID.randomUUID().toString();
                ProduceItem produceItem = realm.createObject(ProduceItem.class, id);
                produceItem.setName(s);
                produceItem.setShoppingListId(category.getId());
            }
        });
    }

    private int getDpsValue(Context context, int dps){
        final float scale = context.getResources().getDisplayMetrics().density;
        int pixels = (int) (dps * scale + 0.5f);
        return pixels;
    }

    private void applySettings() {

        if (sf.getBoolean(C.DISPLAY_ROTATION, false)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        if (sf.getBoolean(C.DISPLAY_TIME_OUT, false)) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }

    }

    public void expendCategory_clickHandler(View view) {
        if (expended){
            openCategoryListView(category, categoryDownBtn, smallWidth, false);
            categoryDownBtn.setImageResource(R.drawable.ic_arrow_right);
            expended = false;
        }else{
            openCategoryListView(category, categoryDownBtn, largeWidth, true);
            categoryDownBtn.setImageResource(R.drawable.ic_arrow_left);
            expended = true;
        }
    }

}
