package com.buddy.grocerybuddy.lib;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.buddy.grocerybuddy.HomeActivity;
import com.buddy.grocerybuddy.SettingActivity;

/**
 * Created by Arif Iqbal on 24-May-17.
 */

public class SimpleDialog extends DialogFragment {

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle("Application Settings");
        builder.setMessage("Do you really want to make changes in you Application setting");

        builder.setPositiveButton("Open", new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialog, int which){
                startActivity(new Intent(getContext(), SettingActivity.class));
//                Log.i("DLG: ","You Clicked Positive Button");
            }
        });

        builder.setNegativeButton("No", new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialog, int which){
//                Log.i("DLG: ","You Clicked Negative Button");
            }
        });

        builder.setNeutralButton("Cancel", new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialog, int which){
//                Log.i("DLG: ","You Clicked Neutral Button");
            }
        });

        return builder.create();
    }
}
