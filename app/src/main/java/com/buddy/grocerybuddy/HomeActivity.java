package com.buddy.grocerybuddy;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.buddy.grocerybuddy.lib.C;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

import io.realm.Realm;

public class HomeActivity extends AppCompatActivity {

    private static final int PICKFILE_RESULT_CODE = 001;
    private EditText noteEditor;
    private ImageButton saveNoteBtn;
    private ImageButton cancelNoteBtn;
    private SharedPreferences sf;
    Realm myRealm;

    private AdView mAdView;
    private String TAG = ShoppingActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setLogo(R.mipmap.gb_icon_header);
        toolbar.setPadding(5,10,5,10);

        MobileAds.initialize(this, getString(R.string.AddMobAppID));

        // Apply User's Setting By Default
        sf = getSharedPreferences(getPackageName() + C.PREF_FILE_NAME, Context.MODE_PRIVATE);
        applySettings();

        myRealm = Realm.getDefaultInstance();
        noteEditor = (EditText) findViewById(R.id.noteEditor);
        saveNoteBtn = (ImageButton) findViewById(R.id.saveNoteBtn);
        cancelNoteBtn = (ImageButton) findViewById(R.id.cancelNoteBtn);

        noteEditor.setText(sf.getString(C.NOTES_EDITOR, ""));
        noteEditor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                noteEditor.setCursorVisible(true);
            }
        });
        watchNoteEdit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        String msg = "You selected ";
        int id = item.getItemId();

        switch (id){
            case R.id.action_edit:
                startActivity(new Intent(HomeActivity.this, SettingActivity.class));
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        applySettings();
    }

    public void updateGroceryList_click_handler(View view) {
        startActivity( new Intent(HomeActivity.this, UpdateGroceryActivity.class) );
    }

    public void startShopping_click_handler(View view) {
        startActivity( new Intent(HomeActivity.this, ShoppingActivity.class) );
    }

    public void EditorFocusBtn_click_handler(View view) {
        noteEditor.setFocusableInTouchMode(true);
        noteEditor.requestFocus();
    }

    public void watchNoteEdit(){

        noteEditor.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if ( noteEditor.getText().length() > 0 ){
                    saveNoteBtn.setVisibility(View.VISIBLE);
                    cancelNoteBtn.setVisibility(View.VISIBLE);
                }else{
                    saveNoteBtn.setVisibility(View.GONE);
                    cancelNoteBtn.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {}

        });

    }

    public void cancelNoteBtnClicked(View view) {

        noteEditor.setFocusable(false);
        noteEditor.setText(sf.getString(C.NOTES_EDITOR, ""));
        saveNoteBtn.setVisibility(View.GONE);
        cancelNoteBtn.setVisibility(View.GONE);

        hideKeyboard();
    }

    private void applySettings() {

        if (sf.getBoolean(C.DISPLAY_ROTATION, false)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        if (sf.getBoolean(C.DISPLAY_TIME_OUT, false)) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }

    }

    public void saveNoteBtnClicked(View view) {
        SharedPreferences.Editor editor = sf.edit();
        editor.putString( C.NOTES_EDITOR, noteEditor.getText().toString() );
        editor.apply();

        saveNoteBtn.setVisibility(View.GONE);
        cancelNoteBtn.setVisibility(View.GONE);
        Toast.makeText(HomeActivity.this, "Note Save Successfully",Toast.LENGTH_LONG).show();
        hideKeyboard();
    }

    private void hideKeyboard() {
        noteEditor.setFocusable(false);
        noteEditor.setFocusableInTouchMode(true);
        noteEditor.setCursorVisible(false);
        InputMethodManager imm = (InputMethodManager)getSystemService(this.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(noteEditor.getWindowToken(), 0);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        myRealm.close();
    }

}
