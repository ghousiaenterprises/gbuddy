package com.buddy.grocerybuddy;

import android.app.Application;

import com.buddy.grocerybuddy.lib.MyRealmMigration;
import com.buddy.grocerybuddy.models.Note;
import com.buddy.grocerybuddy.models.ProduceItem;
import com.buddy.grocerybuddy.models.ShoppingList;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.annotations.RealmModule;

/**
 * Created by Arif Iqbal on 22-May-17.
 */

public class MyApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(this);

        RealmConfiguration con = new RealmConfiguration.Builder()
                .name("myRealm.realm")
                .modules(new MyCustomModule())
                .schemaVersion(1)
                .migration(new MyRealmMigration())
                .build();

        Realm.setDefaultConfiguration(con);
    }

    @RealmModule (classes = {Note.class, ShoppingList.class, ProduceItem.class})
    class MyCustomModule{}
}
