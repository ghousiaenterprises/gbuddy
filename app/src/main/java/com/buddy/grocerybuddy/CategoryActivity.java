package com.buddy.grocerybuddy;

import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.WindowManager;

import com.buddy.grocerybuddy.adapter.RecyclerAdapter;
import com.buddy.grocerybuddy.lib.C;
import com.buddy.grocerybuddy.models.ShoppingList;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import io.realm.Case;
import io.realm.Realm;
import io.realm.RealmResults;

public class CategoryActivity extends AppCompatActivity {

    SharedPreferences sf;
    Realm mReal;
    private AdView mAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setLogo(R.mipmap.manage_produced);
        toolbar.setTitleMargin(50,0,50,0);
        setSupportActionBar(toolbar);

        applySettings();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setRecyclerView();
    }

    private void setRecyclerView() {
        mReal = Realm.getDefaultInstance();
        RealmResults<ShoppingList> ds = mReal.where(ShoppingList.class)
                .notEqualTo("title","Favorite", Case.INSENSITIVE)
                .findAllSorted("position");

        LinearLayoutManager mLinearLayoutManagerVertical = new LinearLayoutManager(this);
        mLinearLayoutManagerVertical.setOrientation(LinearLayoutManager.VERTICAL);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        RecyclerAdapter adapter = new RecyclerAdapter(this, ds, mReal, mLinearLayoutManagerVertical);
        recyclerView.setAdapter(adapter);

        recyclerView.setLayoutManager(mLinearLayoutManagerVertical);
    }

    private void applySettings() {

        sf = getSharedPreferences(getPackageName()+C.PREF_FILE_NAME, MODE_PRIVATE);

        if (sf.getBoolean(C.DISPLAY_ROTATION, false)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        if (sf.getBoolean(C.DISPLAY_TIME_OUT, false)) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mReal.close();
    }

    @Override
    protected void onResume() {
        super.onResume();
        applySettings();
    }

}
