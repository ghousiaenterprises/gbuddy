package com.buddy.grocerybuddy.models;

import com.buddy.grocerybuddy.R;

import java.util.ArrayList;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

/**
 * Created by Arif Iqbal on 22-May-17.
 */

public class ShoppingList extends RealmObject {

    @PrimaryKey
    private String id;
    @Required
    private String title;

    private int imageId;

    private int position;

    private int color;

    private int border;

    private int producedBackground;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public int getBorder() {
        return border;
    }

    public void setBorder(int border) {
        this.border = border;
    }

    public int getProducedBackground() {
        return producedBackground;
    }

    public void setProducedBackground(int producedBackground) {
        this.producedBackground = producedBackground;
    }

    public static ArrayList<ShoppingList> getData(){

        ArrayList<ShoppingList> dataList = new ArrayList<>();

        int[] images = getImages();
        int[] colors = getColors();
        int[] borders = getBorders();
        int[] producedBackground = getProducedBackgroundResouce();
        String[] titles = getTitles();
        int p=0;
        int counter = 1;
        for (int i=0; i < images.length; i++){
            String id = "category_"+(counter++);
            ShoppingList item = new ShoppingList();
            item.setId(id);
            item.setImageId( images[i] );
            item.setTitle( titles[i] );
            item.setColor( colors[i] );
            item.setBorder( borders[i] );
            item.setPosition((p++));
            item.setProducedBackground(producedBackground[i]);

            dataList.add(item);
        }

        return dataList;
    }

    private static int[] getImages() {
        int[] img = {
                R.mipmap.own,
                R.mipmap.fnv,
                R.mipmap.bnp,
                R.mipmap.mnc,
                R.mipmap.mnf,
                R.mipmap.ins,
                R.mipmap.fnc,
                R.mipmap.gp,
                R.mipmap.sns,
                R.mipmap.bnt,
                R.mipmap.house_hold,
                R.mipmap.pet,
                R.mipmap.own
        };
        return img;
    }

    private static int[] getColors() {
        int[] color = {
                R.color.list_blue,
                R.color.list_green,
                R.color.list_pink_dark,
                R.color.list_golden,
                R.color.list_brown_dark,
                R.color.list_blue,
                R.color.list_purple,
                R.color.list_brown_light,
                R.color.list_pink_light,
                R.color.list_blue_navy,
                R.color.list_purple_light,
                R.color.list_grey,
                R.color.list_blue
        };
        return color;
    }

    private static int[] getBorders() {
        int[] color = {
                R.drawable.border_radius_blue,
                R.drawable.border_radius_green,
                R.drawable.border_radius_pink_dark,
                R.drawable.border_radius_golden,
                R.drawable.border_radius_brown_dark,
                R.drawable.border_radius_blue,
                R.drawable.border_radius_purple,
                R.drawable.border_radius_brown_light,
                R.drawable.border_radius_pink_light,
                R.drawable.border_radius_blue_navy,
                R.drawable.border_radius_purple_light,
                R.drawable.border_radius_grey,
                R.drawable.border_radius_blue
        };
        return color;
    }

    private static int[] getProducedBackgroundResouce() {
        int[] bg = {
                R.drawable.produced_background_blue,
                R.drawable.produced_background_green,
                R.drawable.produced_background_pink_dark,
                R.drawable.produced_background_golden,
                R.drawable.produced_background_brown_dark,
                R.drawable.produced_background_blue,
                R.drawable.produced_background_purple,
                R.drawable.produced_background_brown_light,
                R.drawable.produced_background_pink_light,
                R.drawable.produced_background_blue_navy,
                R.drawable.produced_background_purple_light,
                R.drawable.produced_background_grey,
                R.drawable.produced_background_blue
        };
        return bg;
    }

    private static String[] getTitles(){
        String[] titles = {
                "Favorite",
                "Fruits & Vegetables",
                "Bread & Pastries",
                "Mild & Cheese",
                "Meat & Fish",
                "Ingredients & Spices",
                "Frozen & Convenience",
                "Grain Products",
                "Snacks & Sweets",
                "Beverages & Tobacco",
                "Household & Health",
                "Pet Supplies",
                "Own Items"
        };
        return titles;
    }
}
