package com.buddy.grocerybuddy;

import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import com.buddy.grocerybuddy.adapter.ShoppingRecyclerAdapter;
import com.buddy.grocerybuddy.lib.C;
import com.buddy.grocerybuddy.models.ProduceItem;
import com.buddy.grocerybuddy.models.ShoppingList;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.Sort;

public class ShoppingActivity extends AppCompatActivity {

    private static final String TAG = "ShoppingActivity";
    private Realm mRealm;
    private List<ProduceItem> data;
    private ShoppingRecyclerAdapter adapter;
    private boolean showAll = false;
    private boolean showShopped = false;
    private SharedPreferences sf;
    private InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shopping);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setBackgroundResource(R.color.list_blue);
        toolbar.setTitle("Start Shopping");
        toolbar.setNavigationIcon(R.mipmap.btn_ss);

        toolbar.setBackgroundResource(R.color.colorPrimary);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }
        setSupportActionBar(toolbar);

        sf = getSharedPreferences(getPackageName() + C.PREF_FILE_NAME, MODE_PRIVATE);

        mRealm = Realm.getDefaultInstance();
        setupRecyclerView();
        applySettings();
    }

    private void setupRecyclerView() {
        data = getProducedByCategorySortingOrder();
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.shoppingRecyclerView);
        adapter = new ShoppingRecyclerAdapter(getApplicationContext(), data, mRealm);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
    }

    public void showAllClickHandler(View view) {
        showAllSelectedProduced();
    }

    public void showShoppedClickHandler(View view) {
        data = getProducedByCategorySortingOrderOnlyShopped();
        adapter.updateAdapter(data);
    }

    private void showAllSelectedProduced() {
        data = getProducedByCategorySortingOrder();
        adapter.updateAdapter(data);
    }

    public void showStarredClickHandler(View view) {
        data = getProducedByCategorySortingOrderOnlyStarred();
        adapter.updateAdapter(data);
        showAll = !showAll;
    }

    private List<ProduceItem> getProducedByCategorySortingOrderOnlyStarred(){
        List<ProduceItem> list = new ArrayList<>();

        RealmResults<ShoppingList> category = mRealm.where(ShoppingList.class).findAllSorted("position", Sort.ASCENDING);

        for (ShoppingList cat : category) {
            RealmQuery<ProduceItem> query = mRealm.where(ProduceItem.class)
                    .equalTo("shoppingListId", cat.getId())
                    .equalTo("selected",true)
                    .equalTo("starred", true)
                    .equalTo("shoppingCompleted",false);

            RealmResults<ProduceItem> producedItems = query.findAll();

            for (ProduceItem produced : producedItems) {
                list.add(produced);
            }
        }
        return list;
    }

    private List<ProduceItem> getProducedByCategorySortingOrder(){
        List<ProduceItem> list = new ArrayList<>();

        RealmResults<ShoppingList> category = mRealm.where(ShoppingList.class).findAllSorted("position", Sort.ASCENDING);

        for (ShoppingList cat : category) {
            RealmQuery<ProduceItem> query = mRealm.where(ProduceItem.class)
                    .equalTo("shoppingListId", cat.getId())
                    .equalTo("selected",true)
                    .equalTo("shoppingCompleted",false);

            RealmResults<ProduceItem> producedItems = query.findAll();

            for (ProduceItem produced : producedItems) {
                list.add(produced);
            }
        }
        return list;
    }

    private List<ProduceItem> getProducedByCategorySortingOrderOnlyShopped(){
        List<ProduceItem> list = new ArrayList<>();

        RealmResults<ShoppingList> category = mRealm.where(ShoppingList.class).findAllSorted("position", Sort.ASCENDING);

        for (ShoppingList cat : category) {
            RealmQuery<ProduceItem> query = mRealm.where(ProduceItem.class)
                    .equalTo("shoppingListId", cat.getId())
                    .equalTo("selected",true)
                    .equalTo("shoppingCompleted",true);

            RealmResults<ProduceItem> producedItems = query.findAll();

            for (ProduceItem produced : producedItems) {
                list.add(produced);
            }
        }
        Log.d(TAG, "getProducedByCategorySortingOrderOnlyShopped: Function ran");
        return list;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mRealm.close();
    }

    @Override
    protected void onResume() {
        super.onResume();
        applySettings();
    }

    public void clearShoppingList_ClickHandler(View view) {

        showPromotionAdd();
        mRealm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<ProduceItem> items = realm.where(ProduceItem.class).equalTo("shoppingCompleted", true).findAll();
                for (ProduceItem item : items) {
                    item.setShoppingCompleted(false);
                    item.setSelected(false);
                }
//                    item.setStarred(false);
            }
        });
        data = getProducedByCategorySortingOrder();
        adapter.updateAdapter(data);
    }

    private void applySettings() {

        if (sf.getBoolean(C.DISPLAY_ROTATION, false)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        if (sf.getBoolean(C.DISPLAY_TIME_OUT, false)) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }

    }

    private void showPromotionAdd() {
        mInterstitialAd = new InterstitialAd(this);
        // set the ad unit ID
        mInterstitialAd.setAdUnitId(getString(R.string.AddMobAppFullScreenUnitID));
        AdRequest adRequestFull = new AdRequest.Builder()
                .build();
        // Load ads into Interstitial Ads
        mInterstitialAd.loadAd(adRequestFull);
        mInterstitialAd.setAdListener(new AdListener() {
            public void onAdLoaded() {
                showInterstitial();
            }
        });
    }

    private void showInterstitial() {
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        }
    }

}
