package com.buddy.grocerybuddy.models;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

/**
 * Created by Arif Iqbal on 22-May-17.
 */

public class Note extends RealmObject {

    @PrimaryKey
    private String id;
    @Required
    private String noteText;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNoteText() {
        return noteText;
    }

    public void setNoteText(String noteText) {
        this.noteText = noteText;
    }
}
