package com.buddy.grocerybuddy.lib;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;

import com.buddy.grocerybuddy.R;

/**
 * Created by Arif Iqbal on 24-May-17.
 */

public class CustomDescriptionDialog extends DialogFragment implements View.OnClickListener {

    private int pos;
    private EditText editor;
    private ImageButton saveBtn, cancelBtn;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        pos = getArguments().getInt("position");

        View inflate = getLayoutInflater(savedInstanceState).inflate(R.layout.custom_add_produced_dialog, null);
        editor = (EditText) inflate.findViewById(R.id.description_text_editer);
        saveBtn = (ImageButton) inflate.findViewById(R.id.save_description_btn);
        cancelBtn = (ImageButton) inflate.findViewById(R.id.cancel_description_btn);

        saveBtn.setOnClickListener(this);
        cancelBtn.setOnClickListener(this);

        builder.setView(R.layout.custom_add_produced_dialog);
        return builder.create();
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.cancelNoteBtn:
                break;
        }
    }
}
