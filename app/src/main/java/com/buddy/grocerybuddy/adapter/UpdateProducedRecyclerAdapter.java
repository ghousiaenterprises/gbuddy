package com.buddy.grocerybuddy.adapter;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.buddy.grocerybuddy.R;
import com.buddy.grocerybuddy.UpdateGroceryActivity;
import com.buddy.grocerybuddy.models.ProduceItem;
import com.buddy.grocerybuddy.models.ShoppingList;

import io.realm.Realm;
import io.realm.RealmResults;


/**
 * Created by Arif Iqbal on 22-May-17.
 */

public class UpdateProducedRecyclerAdapter extends RecyclerView.Adapter<UpdateProducedRecyclerAdapter.MyViewHolder> {

    private RealmResults<ProduceItem> data;
    private LayoutInflater mInflater;
    private Boolean displayText = false;
    private Realm mRealm;
    private Context context;
    private ShoppingList currentCategory;

    public UpdateProducedRecyclerAdapter(Context context, RealmResults<ProduceItem> data, Realm mRealmInstance) {
        this.data = data;
        this.mInflater = LayoutInflater.from(context);
        this.mRealm = mRealmInstance;
        this.context = context;
    }

    public void updateData(RealmResults<ProduceItem> data, ShoppingList currentCategory){
        this.data = data;
        this.currentCategory = currentCategory;
        UpdateGroceryActivity.currentSelectedCategory = currentCategory;
        notifyDataSetChanged();
    }

    public void updateData(RealmResults<ProduceItem> data){
        this.data = data;
        notifyDataSetChanged();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.update_grocery_produced_list_item, parent, false);
        MyViewHolder holder = new MyViewHolder(view, mRealm);
        return holder;
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        ProduceItem currentItem = data.get(position);
        holder.setData(currentItem, position);
    }

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private final String TAG =  UpdateProducedRecyclerAdapter.class.getSimpleName();
        TextView title, description;
        ImageButton markStarred, makeNote;
        CheckBox select;
        int position;
        ProduceItem current;

        MyViewHolder(View itemView, Realm realm) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.update_grocery_card_view_title);
            description = (TextView) itemView.findViewById(R.id.update_grocery_card_view_description);
            markStarred = (ImageButton) itemView.findViewById(R.id.makeStarted);
            makeNote = (ImageButton) itemView.findViewById(R.id.makeNote);
            select = (CheckBox) itemView.findViewById(R.id.checkProduce);
            makeNote.setOnClickListener(this);
            markStarred.setOnClickListener(this);
        }

        void setData(ProduceItem i, final int position) {
            title.setText(i.getName());
            description.setText(i.getDescription());
            markStarred.setImageResource( i.isStarred() ? R.mipmap.icn_starred_dis_on : R.mipmap.icn_starred_dis);
            select.setChecked( i.getSelected() );

            select.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Realm r = Realm.getDefaultInstance();
                    final String id = current.getId();
                    r.executeTransactionAsync(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            ProduceItem item = realm.where(ProduceItem.class).equalTo("id", id).findFirst();
                            item.setSelected(!item.getSelected());
                        }
                    });
                    r.close();
                }
            });

            this.position = position;
            this.current = i;
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.makeNote:

                    InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

                    final Dialog dialog;
                    final AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    View inflate = LayoutInflater.from(context).inflate(R.layout.custom_produced_description_dialog, null);
                    builder.setView(inflate);
                    builder.create();
                    dialog = builder.show();
                    final EditText editor = (EditText) inflate.findViewById(R.id.description_text_editer);
                    editor.setText(current.getDescription());
                    ImageButton saveBtn = (ImageButton) inflate.findViewById(R.id.save_description_btn);
                    ImageButton cancelBtn = (ImageButton) inflate.findViewById(R.id.cancel_description_btn);
                    saveBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            saveItemNote(position, editor.getText().toString());
                            if (dialog != null) {
                                dialog.dismiss();
                            }
                            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.toggleSoftInput(InputMethodManager.HIDE_NOT_ALWAYS, 0);
                        }
                    });
                    cancelBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (dialog != null) {
                                dialog.dismiss();
                            }
                            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.toggleSoftInput(InputMethodManager.HIDE_NOT_ALWAYS, 0);
                        }
                    });
                    break;
                case R.id.makeStarted:
                    markAsStarred(position);
                    break;
            }

        }

        public void markAsStarred(final int pos){

            final ProduceItem currentItem = data.get(pos);
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    ProduceItem item = realm.where(ProduceItem.class).equalTo("id", currentItem.getId()).findFirst();
                    item.setStarred( !currentItem.isStarred() );
                }
            });
            notifyItemChanged(pos);
        }

        public void markAsSelected(final int pos){

            final ProduceItem currentItem = data.get(pos);
            mRealm.executeTransactionAsync(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    ProduceItem item = realm.where(ProduceItem.class).equalTo("id", currentItem.getId()).findFirst();
                    item.setSelected( currentItem.getSelected() );
                }
            });
            notifyItemChanged(pos);
        }

        public void saveItemNote(final int pos, final String description){

            final ProduceItem currentItem = data.get(pos);
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    ProduceItem item = realm.where(ProduceItem.class).equalTo("id", currentItem.getId()).findFirst();
                    item.setDescription(description);
                }
            });
            notifyItemChanged(pos);
        }

    }

}
