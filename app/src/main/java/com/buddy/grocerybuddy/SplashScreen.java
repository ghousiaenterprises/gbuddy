package com.buddy.grocerybuddy;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.buddy.grocerybuddy.lib.C;
import com.buddy.grocerybuddy.models.ProduceItem;
import com.buddy.grocerybuddy.models.ShoppingList;

import java.util.ArrayList;

import io.realm.Realm;

public class SplashScreen extends AppCompatActivity {

    private static final int SPLASH_TIMEOUT = 1500;
    private static final String FIRST_RUN = "first_run";
    private static final String TAG = "SplashActivity";
    private Realm mReal;
    private SharedPreferences sf;
    private Handler handler;
    private Runnable runnable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        mReal = Realm.getDefaultInstance();
        sf = getSharedPreferences(getPackageName()+ C.PREF_FILE_NAME, MODE_PRIVATE);

        insertData();

        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(SplashScreen.this, HomeActivity.class);
                startActivity(intent);
                finish();
            }
        };

        handler.postDelayed(runnable, SPLASH_TIMEOUT);
    }

    private void insertData(){

        if( sf.getBoolean(FIRST_RUN, true) ){
            ArrayList<ShoppingList> lists = ShoppingList.getData();
            for (ShoppingList item : lists){
                final ShoppingList i = item;
                mReal.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        ShoppingList listItem = mReal.createObject(ShoppingList.class, i.getId());
                        listItem.setPosition(i.getPosition());
                        listItem.setImageId(i.getImageId());
                        listItem.setTitle(i.getTitle());
                        listItem.setColor(i.getColor());
                        listItem.setBorder(i.getBorder());
                        listItem.setProducedBackground(i.getProducedBackground());
                    }
                });
            }

            ArrayList<ProduceItem> produceItems = ProduceItem.getData();
            for (ProduceItem item: produceItems) {
                final ProduceItem i = item;
                mReal.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        ProduceItem listItem = mReal.createObject(ProduceItem.class, i.getId());
                        listItem.setName(i.getName());
                        listItem.setDescription(i.getDescription());
                        listItem.setShoppingListId(i.getShoppingListId());
                        listItem.setSelected(i.getSelected());
                        listItem.setStarred(i.isStarred());
                        listItem.setShoppingCompleted(i.isShoppingCompleted());
                    }
                });
            }

            SharedPreferences.Editor editor = sf.edit();
            editor.putBoolean(FIRST_RUN,false);
            editor.apply();

        }else{

            ArrayList<ShoppingList> lists = ShoppingList.getData();
            for (ShoppingList item : lists){
                final ShoppingList i = item;
                mReal.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        ShoppingList listItem = realm.where(ShoppingList.class)
                                .equalTo("id", i.getId())
                                .findFirst();
                        listItem.setImageId(i.getImageId());
                        listItem.setColor(i.getColor());
                        listItem.setBorder(i.getBorder());
                        listItem.setProducedBackground(i.getProducedBackground());
                    }
                });
            }
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mReal.close();
        handler.removeCallbacks(runnable);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
