package com.buddy.grocerybuddy.models;

import java.util.ArrayList;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

/**
 * Created by Arif Iqbal on 25-May-17.
 */

public class ProduceItem extends RealmObject {

    @PrimaryKey
    private String id;
    @Required
    private String name;
    @Required
    private String shoppingListId;
    private boolean shoppingCompleted = false;
    private boolean starred;
    private boolean selected;
    private String description;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShoppingListId() {
        return shoppingListId;
    }

    public void setShoppingListId(String shoppingListId) {
        this.shoppingListId = shoppingListId;
    }

    public boolean isStarred() {
        return starred;
    }

    public void setStarred(boolean starred) {
        this.starred = starred;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean getSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public boolean isShoppingCompleted() {
        return shoppingCompleted;
    }

    public void setShoppingCompleted(boolean shoppingCompleted) {
        this.shoppingCompleted = shoppingCompleted;
    }

    public static ArrayList<ProduceItem> getData(){

        ArrayList<ProduceItem> dataList = new ArrayList<>();

        String[][] categoryWiseProduced = getProducedTitles();
        int i = 1;
        int j=1;
        for (String[] producedArray : categoryWiseProduced) {
            for (String producedTitle : producedArray) {
                ProduceItem p = new ProduceItem();
                p.setId("item_"+(j++));
                p.setName(producedTitle);
                p.setDescription("");
                p.setShoppingListId("category_"+i);
                p.setShoppingCompleted(false);
                p.setSelected(false);
                p.setStarred(false);
                dataList.add(p);
            }
            i++;
        }

        return dataList;
    }

    public static String[][] getProducedTitles(){

        String[][] category = {
                new String[]{},
                new String[]{
                        "Apples", "Apricot", "Artichokes",
                        "Arugula", "Asparagus", "Avocado",
                        "Bananas", "Basil", "Beets",
                        "Bell pepper", "Berries", "Blackberries",
                        "Blueberries", "Broccoli", "Cabbage",
                        "Carrots", "Cauliflower", "Celery",
                        "Cherries", "Cherry tomatoes", "Chillies",
                        "Chives", "Cilantro", "Cranberries",
                        "Cucumber", "Dates", "Eggplant",
                        "Fennel", "Figs", "Fruits",
                        "Garlic", "Ginger", "Grapefruit",
                        "Grapes", "Herbs", "Kiwis",
                        "Leek", "Lemon", "Lettuce",
                        "Lime", "Mandarins", "Mango",
                        "Melon", "Mint", "Mushrooms",
                        "Nectarine", "Olives", "Onions",
                        "Orange", "Parsley", "Passion fruit",
                        "Peach", "Pears", "Peas",
                        "Pineapple", "Potatoes", "Prunes",
                        "Radish", "Raspberries", "Rhubarb",
                        "Sage", "Salad", "Scallions",
                        "Spinach", "Squash", "Strawberries",
                        "Sun-dried tomatoes", "Sweet Corn", "Sweet Potatoes",
                        "Thyme", "Tomatoes", "Vegetables",
                        "Watermelon", "Zucchini"
                },
                new String[]{
                        "Bagels", "Baguette", "Bread",
                        "Buns", "Crispbread", "Croissant",
                        "Dinner Rolls", "Donuts", "Muffins",
                        "Pancakes mix", "Pie", "Pizza dough",
                        "Puff pastry", "Pumpkin Pie", "Rolls",
                        "Scones", "Sliced Bread", "Toast",
                        "Tortillas", "Waffles"
                },
                new String[]{
                        "Blue cheese", "Butter", "Cheddar",
                        "Cheese", "Cottage Cheese", "Cream",
                        "Cream cheese", "Creme fraiche", "Eggs",
                        "Feta", "Gorgonzola", "Grated cheese",
                        "Margarine", "Mascarpone", "Milk",
                        "Mozzarella", "Parmesan", "Quark",
                        "Ricotta", "Sour Cream", "Soy Milk",
                        "Yogurt"
                },
                new String[]{
                        "Anchovies", "Bacon", "Beef",
                        "Bratwurst", "Chicken", "Chicken breast",
                        "Cold cuts", "Fish", "Ground meet",
                        "Ham", "Hot dog", "Lamb",
                        "Lobster", "Meat", "Mussels",
                        "Oysters", "Pork", "Prosciutto",
                        "Salami", "Salmon", "Sausage",
                        "Sausages", "Shrimp", "Sliced beef",
                        "Steak", "Tuna", "Turkey",
                        "Turkey breast", "Veal"
                },
                new String[]{
                        "Almonds", "Apple sauce", "Baking powder",
                        "Baking soda", "Balsamic", "BBQ sauce",
                        "Beans", "Breadcrumbs", "Canned tomatoes",
                        "Chutney", "Cinnamon", "Coconut milk",
                        "Cornbread", "Cornflour", "Cranberry sauce",
                        "Dip", "Gravy", "Hazelnuts",
                        "Hot sauce", "Icing sugar", "Ketchup",
                        "Lentils", "Maple syrup", "Mashed Potatoes",
                        "Mayonnaise", "Mustard", "Nuts",
                        "Oil", "Olive oil", "Oregano",
                        "Paprika", "Pasta sauce", "Peanut butter",
                        "Peppercorns", "Pine nuts", "Rosemary",
                        "Salad dressing", "Salt", "Soy sauce",
                        "Stock", "Sugar", "Tomato puree",
                        "Tomato sauce", "Vanilla", "Vinegar",
                        "Walnuts", "Yeast"
                },
                new String[]{
                        "Baked Beans", "Burritos", "Chicken Wings",
                        "Chinese foods", "Dumplings", "Fish sticks",
                        "French fries", "Frozen vegetables", "Ice cream",
                        "Indian foods", "Italian foods", "Lasagna",
                        "Mexican foods", "Pizza", "Soup",
                        "Thai foods", "TV dinner"
                },
                new String[]{
                        "Basmati rice", "Cereal", "Chickpeas",
                        "Corn flakes", "Couscous", "Flour",
                        "Muesli", "Noodles", "Oatmeal",
                        "Pasta", "Penne", "Rice",
                        "Risotto rice", "Semolina", "Spaghetti",
                        "Tofu", "Wild rice"
                },
                new String[]{
                        "Cake", "Candy",
                        "Cereal bar", "Chewing gum", "Chips",
                        "Chocolate", "Christmas cookies", "Cookies",
                        "Crackers", "Dessert", "Died fruit",
                        "Gingerbread", "Honey", "Jam",
                        "Jello", "Marshmallows", "Nougat cream",
                        "Peanuts", "Pop corn", "Pretzels",
                        "Snacks", "Thank you", "Tortilla chips"
                },
                new String[]{
                        "Apple juice", "Beer", "Beverages",
                        "Bottled water", "Champagne", "Cider",
                        "Cigarettes", "Coffee", "Cola",
                        "Diet Cola", "Diet soda", "Energy drink",
                        "Fruit juice", "Gin", "Ginger Ale",
                        "Hot chocolate", "Iced tea", "Orange juice",
                        "Prosecco", "Red wine", "Rum",
                        "Smoothie", "Soda", "Spirits",
                        "Sports drink", "Tea", "Tonic Water",
                        "Vodka", "Water", "Whiskey",
                        "White wine"
                },
                new String[]{
                        "Aluminum foil", "Baby food", "Bandaids",
                        "Bathroom cleaner", "Batteries", "Body lotion",
                        "Candles", "Charcoal", "Cleaning supplies",
                        "Conditioner", "Condoms", "Contact lens solution",
                        "Cotton pads", "Cotton swabs", "Dental floss",
                        "Deodorant", "Diapers", "Dishwasher salt",
                        "Dishwasher tabs", "Dishwashing liquid", "Febric softener",
                        "Face cream", "Facial tissues", "Flowers",
                        "Garbage bags", "Gift", "Glass cleaner",
                        "Hair gel", "Hair spray", "Hand cream",
                        "Insect Repellent", "Laundry detergent", "Lib balm",
                        "Light bulb", "Makeup remover", "Mouthwash",
                        "Nail polish", "Nail polish remover", "Napkins",
                        "pads", "Pain reliever", "Paper towels",
                        "Perchment paper", "Plastic wrap", "Potting soil",
                        "Propane", "Razor", "Razor blades",
                        "Shampoo", "Shaving cream", "Shower gel",
                        "Soap", "Sponge", "Sunblock",
                        "Tampons", "Tissues", "Toilet cleaner",
                        "Toilet paper", "Toothbrush", "Toothpaste",
                        "Vitamins", "Wet wipes", "Wrapping paper"
                },
                new String[]{
                        "Bird food", "Cat food", "Cat litter",
                        "Cat treats", "Dog food", "Dog treats",
                        "Fish foods"
                }
        };

        return category;
    }

}
